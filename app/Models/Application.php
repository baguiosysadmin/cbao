<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{	


	protected $fillable = [
        'application_no', 'name', 'status_id', 'official_id', 'type_id', 'location'
    ];

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'official_id');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type', 'type_id');
    }

   public function barangay()
    {
        return $this->belongsTo('App\Models\Barangay', 'barangay_id');
    }

     public function building()
    {
        return $this->belongsTo('App\Models\Building', 'building_id');
    }

    public function trackings()
    {
        return $this->hasMany('App\Models\Tracking');
    }

}
