<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function application()
    {
        return $this->belongsTo('App\Models\Application', 'application_id');
    }
}
