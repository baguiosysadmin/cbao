<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingTypeSub extends Model
{
    public function building_type()
    {
        return $this->belongsTo('App\Models\BuildingType', 'building_type_id');
    }
}
