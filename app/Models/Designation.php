<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = [
        'name', 'description',
    ];

    public function designations()
    {
        return $this->hasMany('App\Models\Designation');
    }
}
