<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'official_id');
    }

    public function project_trackings()
    {
        return $this->hasMany('App\Models\projectTracking');
    }
}
