<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    public function building_type_subs()
    {
        return $this->hasMany('App\Models\BuildingTypeSub');
    }
}
