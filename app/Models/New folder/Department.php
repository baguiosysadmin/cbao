<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    public function official()
    {
        return $this->belongsTo('App\Models\Official');
    }

    protected $fillable = [
        'name', 'room', 'location', 'contact_number', 'official_id'
    ];
}
