<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Official extends Model
{
    public function departments()
    {
	    return $this->hasMany('App\Models\Department');
	}
	protected $fillable = [
        'prefix', 'name', 'position', 'description',
    ];
}
