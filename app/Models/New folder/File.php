<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    public function charter()
    {
        return $this->belongsTo('App\Models\Charter', 'charter_id');
    }
}
