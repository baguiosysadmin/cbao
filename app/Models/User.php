<?php

namespace App\Models;

use App\Notifications\PasswordResetNotification;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'title_id', 'role_id', 'firstname', 'middlename', 'lastname', 'barangay_id', 'citenship', 'contact_no', 'home_address', 'active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','activation_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function title()
    {
        return $this->belongsTo('App\Models\Title', 'title_id');
    }
    public function role()
    {
        return $this->belongsTo('App\Models\Role', 'role_id');
    }
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }
    public function division()
    {
        return $this->belongsTo('App\Models\Division', 'division_id');
    }
    public function trackings()
    {
        return $this->hasMany('App\Models\Tracking');
    }
    public function projectTrackings()
    {
        return $this->hasMany('App\Models\projectTracking');
    }
}
