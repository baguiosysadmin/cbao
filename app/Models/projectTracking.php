<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class projectTracking extends Model
{
    public function project()
    {
        return $this->belongsTo('App\Models\Project', 'project_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }
}
