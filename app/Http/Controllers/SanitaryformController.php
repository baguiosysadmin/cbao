<?php

namespace App\Http\Controllers;

use App\Sanitaryform;
use Illuminate\Http\Request;

class SanitaryformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sanitaryform  $sanitaryform
     * @return \Illuminate\Http\Response
     */
    public function show(Sanitaryform $sanitaryform)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sanitaryform  $sanitaryform
     * @return \Illuminate\Http\Response
     */
    public function edit(Sanitaryform $sanitaryform)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sanitaryform  $sanitaryform
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sanitaryform $sanitaryform)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sanitaryform  $sanitaryform
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sanitaryform $sanitaryform)
    {
        //
    }
}
