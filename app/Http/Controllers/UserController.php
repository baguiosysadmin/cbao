<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index() {
    	return view("layouts.users");
    }

    public function create() {
    	return view("layouts.user-create");
    }

    public function edit($id) {
    	return view("layouts.user-edit", compact('id'));
    }
}
