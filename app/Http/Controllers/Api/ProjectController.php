<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::with('status','user')->orderBy('created_at', 'desc')->get();
        $projects->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($projects)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'job_order_no' => 'required|unique:projects',
            'client' => 'required',
            'date_time' => 'required',
            'title' => 'required'

        ]);
        
        $project = new Project();

        $project->job_order_no = strtoupper($request->get('job_order_no'));
        $project->client = strtoupper($request->get('client'));
        $project->status_id = $request->get('status_id');
        $project->official_id = $request->get('official_id');
        $project->title = strtoupper($request->get('title'));
        $project->date_time = strtoupper($request->get('date_time'));

        $project->save();

        return response()->json($project);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::with('status', 'user')->find($id);
        $project->load(['project_trackings' => function($query) {
            $query->with('user','status')->orderBy('created_at', 'asc');
        }]);
        return response()->json($project);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'status_id' => 'required',

        ]);
        
        $project = Project::find($id);
        $project->job_order_no = strtoupper($request->get('job_order_no'));
        $project->client = strtoupper($request->get('client'));
        $project->status_id = $request->get('status_id');
        $project->official_id = $request->get('official_id');
        $project->title = strtoupper($request->get('title'));
        $project->date_time = $request->get('date_time');

        $project->update();
        return response()->json($project);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }

    public function queuing(Request $request) 
    {   
        $projects = Project::with('status', 'user')->orderBy('created_at', 'asc')->where([['status_id', '!=', 7], ['status_id', '!=', 8]])->get();
        $projects->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($projects)->make(true);
    }
    public function process(Request $request) 
    {   
        $projects = Project::with('status', 'user')->where('official_id', $request->user_id)->get();
        $projects->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($projects)->make(true);
    }
    public function compliance(Request $request) 
    {
        $projects = Project::with('status', 'user')->orderBy('created_at', 'asc')->where([['status_id', '=', 4], ['official_id', $request->user_id]])->get();
        $projects->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($projects)->make(true);
    }
    public function legal(Request $request) 
    {
        $projects = Project::with('status', 'user')->where([['status_id', '=', 10], ['official_id', $request->user_id]])->get();
        return datatables($projects)->make(true);
    }
}
