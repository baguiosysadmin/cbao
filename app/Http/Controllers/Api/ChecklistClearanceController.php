<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistClearance;
use Illuminate\Support\Str;

class ChecklistClearanceController extends Controller
{
    public function store(Request $request) 
    {  
        $this->validate($request, [
            'clearance_other' => 'required',
            'clearance_other.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('clearance_other');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistClearance();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistClearance::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistClearance::find($id);
        $file->delete();
        return response()->json($file);
    }
}
