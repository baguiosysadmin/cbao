<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Tracking;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
class ApplicationController extends Controller
{
    public function index() 
    {

        /*$users = User::orderBy("id", "DESC")->paginate(5);*/
        // $applications = Application::with('status', 'type', 'user')->get();
        /*return response()->json($users);*/
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'desc')->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function store(Request $request) 
    {
        $this->validate($request, [
            'application_no' => 'required|unique:applications',
            'name' => 'required',
            'contact_no' => 'required',
            'representative' => 'required',
/*            'type_id' => 'required',*/
            'location' => 'required',
            'barangay_id' => 'required',
            'building_id' => 'required',

        ]);
        
        $application = new Application();

        $application->application_no = strtoupper($request->get('application_no'));
        $application->name = strtoupper($request->get('name'));
        $application->status_id = $request->get('status_id');
        $application->type_id = $request->get('type_id');
        $application->contact_no = $request->get('contact_no');
        $application->representative = strtoupper($request->get('representative'));
        $application->official_id = $request->get('official_id');
        $application->location = strtoupper($request->get('location'));
        $application->building_id = $request->get('building_id');
        $application->barangay_id = $request->get('barangay_id');

        $application->save();

        return response()->json($application);

    }

    public function show($id)
    {
        $application = Application::with('status', 'type', 'user', 'building', 'barangay')->find($id);
        $application->load(['trackings' => function($query) {
            $query->with(['user' => function($dd) {
                $dd->with('designation');
            },'status'])->orderBy('created_at', 'asc');
        }]);
        return response()->json($application);
    }
  
    public function update($id, Request $request)
    {  
        $this->validate($request, [
            'status_id' => 'required',

        ]);
        
        $application = Application::find($id);
        $application->application_no = strtoupper($request->get('application_no'));
        $application->name = strtoupper($request->get('name'));
        $application->status_id = $request->get('status_id');
        $application->contact_no = $request->get('contact_no');
        $application->representative = strtoupper($request->get('representative'));
        $application->official_id = $request->get('official_id');
        $application->location = strtoupper($request->get('location'));
        $application->building_id = $request->get('building_id');
        $application->barangay_id = $request->get('barangay_id');

        $application->update();
        return response()->json($application);
    }

    public function return($id, Request $request)
    {  
        $this->validate($request, [
            'status_id' => 'required',

        ]);
        
        $application = Application::find($id);

        $application->application_no = $request->except('application_no');
        $application->name = $request->except('name');
        $application->official_id = $request->except('official_id');
        $application->status_id = $request->get('status_id');
        $application->update($request->all());
        return response()->json($application);
    }

    public function destroy($id)
    {
        $application = Application::find($id);

        $application->delete();

        return response()->json('User Deleted');
    }
    
    public function process(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->where('official_id', $request->user_id)->orWhere('official_id', '=', 1)->get();
        return datatables($applications)->make(true);
    }

    public function compliance(Request $request) 
    {
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '=', 4], ['official_id', $request->user_id]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function legal(Request $request) 
    {
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->where([['status_id', '=', 10], ['official_id', $request->user_id]])->get();
        return datatables($applications)->make(true);
    }

    public function evaluation(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where('status_id', '=', 3)->orWhere('status_id', '=', 13)->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function sorting(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '=', 2]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function approval(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '=', 5]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function approved(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '=', 6]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function released(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('updated_at', 'desc')->where([['status_id', '=', 7]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    public function deficiency(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '=', 4]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

    

    public function signature(Request $request) 
    {
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->whereBetween('status_id', [5,7])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }

   

    public function queuing(Request $request) 
    {   
        $applications = Application::with('status', 'type', 'user', 'building', 'barangay')->orderBy('created_at', 'asc')->where([['status_id', '!=', 7], ['status_id', '!=', 8]])->get();
        $applications->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return datatables($applications)->make(true);
    }
}
