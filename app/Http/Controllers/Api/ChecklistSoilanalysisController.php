<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistSoilanalysis;
use Illuminate\Support\Str;

class ChecklistSoilanalysisController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'soil_analysis' => 'required',
            'soil_analysis.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('soil_analysis');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistSoilanalysis();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistSoilanalysis::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistSoilanalysis::find($id);
        $file->delete();
        return response()->json($file);
    }
}
