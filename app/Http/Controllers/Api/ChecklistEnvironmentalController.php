<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistEnvironmental;
use Illuminate\Support\Str;

class ChecklistEnvironmentalController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'env_certificate' => 'required',
            'env_certificate.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('env_certificate');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistEnvironmental();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistEnvironmental::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistEnvironmental::find($id);
        $file->delete();
        return response()->json($file);
    }
}
