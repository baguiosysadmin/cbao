<?php

namespace App\Http\Controllers\Api;

use App\Models\Electricalform;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ElectricalformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $electrical = Electricalform::all();
        return response()->json($electrical);
    }
    public function client($id)
    {
        $electrical = Electricalform::where('user_id', $id)->get();
        return response()->json($electrical);
    }
    public function slug($slug)
    {
        $electrical = Electricalform::where('tracking_id', $slug)->first();
        return response()->json($electrical);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'total_connected_load' => 'required',
            'total_transformer' => 'required',
            'total_generator_capacity' => 'required',
            'design_electric_last_name' => 'required',
            'design_electric_first_name' => 'required',
            'design_electric_middle_name' => 'required',
            'design_electric_prc' => 'required',
            'design_electric_validity' => 'required',
            'design_electric_ptr' => 'required',
            'design_electric_data_issued' => 'required',
            'design_electric_issued_at' => 'required',
            'design_electric_tin' => 'required',
            'design_electric_address' => 'required',
            'supervisor_electric_last_name' => 'required',
            'supervisor_electric_first_name' => 'required',
            'supervisor_electric_middle_name' => 'required',
            'supervisor_electric_prc' => 'required',
            'supervisor_electric_validity' => 'required',
            'supervisor_electric_ptr' => 'required',
            'supervisor_electric_data_issued' => 'required',
            'supervisor_electric_issued_at' => 'required',
            'supervisor_electric_tin' => 'required',
            'supervisor_electric_address' => 'required',
        ]);
        
        $electrical = new Electricalform();

        $electrical->electric_new_construction = $request->get('electric_new_construction');
        $electrical->electric_erection = $request->get('electric_erection');
        $electrical->electric_addition = $request->get('electric_addition');
        $electrical->electric_alteration = $request->get('electric_alteration');
        $electrical->electric_ancillary_structure = $request->get('electric_ancillary_structure');
        $electrical->electric_renovation = $request->get('electric_renovation');
        $electrical->electric_repair = $request->get('electric_repair');
        $electrical->electric_moving = $request->get('electric_moving');
        $electrical->electric_demolition = $request->get('electric_demolition');
        $electrical->electric_others = $request->get('electric_others');
        $electrical->total_connected_load = strtoupper($request->get('total_connected_load'));
        $electrical->total_transformer = strtoupper($request->get('total_transformer'));
        $electrical->total_generator_capacity = strtoupper($request->get('total_generator_capacity'));
        $electrical->design_electric_last_name = strtoupper($request->get('design_electric_last_name'));
        $electrical->design_electric_first_name = strtoupper($request->get('design_electric_first_name'));
        $electrical->design_electric_middle_name = strtoupper($request->get('design_electric_middle_name'));
        $electrical->design_electric_prc = strtoupper($request->get('design_electric_prc'));
        $electrical->design_electric_validity = strtoupper($request->get('design_electric_validity'));
        $electrical->design_electric_ptr = strtoupper($request->get('design_electric_ptr'));
        $electrical->design_electric_data_issued =strtoupper( $request->get('design_electric_data_issued'));
        $electrical->design_electric_issued_at = strtoupper($request->get('design_electric_issued_at'));
        $electrical->design_electric_tin = strtoupper($request->get('design_electric_tin'));
        $electrical->design_electric_address =strtoupper( $request->get('design_electric_address'));
        $electrical->prof_electrical = $request->get('prof_electrical');
        $electrical->reg_electrical = $request->get('reg_electrical');
        $electrical->reg_master = $request->get('reg_master');
        $electrical->supervisor_electric_last_name = strtoupper($request->get('supervisor_electric_last_name'));
        $electrical->supervisor_electric_first_name = strtoupper($request->get('supervisor_electric_first_name'));
        $electrical->supervisor_electric_middle_name = strtoupper($request->get('supervisor_electric_middle_name'));
        $electrical->supervisor_electric_prc = strtoupper($request->get('supervisor_electric_prc'));
        $electrical->supervisor_electric_validity =strtoupper($request->get('supervisor_electric_validity'));
        $electrical->supervisor_electric_ptr = strtoupper($request->get('supervisor_electric_ptr'));
        $electrical->supervisor_electric_data_issued = strtoupper($request->get('supervisor_electric_data_issued'));
        $electrical->supervisor_electric_issued_at = strtoupper($request->get('supervisor_electric_issued_at'));
        $electrical->supervisor_electric_tin = strtoupper($request->get('supervisor_electric_tin'));
        $electrical->supervisor_electric_address = strtoupper($request->get('supervisor_electric_address'));
        $electrical->user_id = $request->get('user_id');
        $electrical->tracking_id = $request->get('tracking_id');

        $electrical->save();

        return response()->json($electrical);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Electricalform  $electrical
     * @return \Illuminate\Http\Response
     */
    public function show(Electricalform $electrical)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Electricalform  $electrical
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Electricalform  $electrical
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Request $request)
    {
        $this->validate($request, [
            'total_connected_load' => 'required',
            'total_transformer' => 'required',
            'total_generator_capacity' => 'required',
            'design_electric_last_name' => 'required',
            'design_electric_first_name' => 'required',
            'design_electric_middle_name' => 'required',
            'design_electric_prc' => 'required',
            'design_electric_validity' => 'required',
            'design_electric_ptr' => 'required',
            'design_electric_data_issued' => 'required',
            'design_electric_issued_at' => 'required',
            'design_electric_tin' => 'required',
            'design_electric_address' => 'required',
            'supervisor_electric_last_name' => 'required',
            'supervisor_electric_first_name' => 'required',
            'supervisor_electric_middle_name' => 'required',
            'supervisor_electric_prc' => 'required',
            'supervisor_electric_validity' => 'required',
            'supervisor_electric_ptr' => 'required',
            'supervisor_electric_data_issued' => 'required',
            'supervisor_electric_issued_at' => 'required',
            'supervisor_electric_tin' => 'required',
            'supervisor_electric_address' => 'required',
        ]);
        
        $electrical = Electricalform::where('tracking_id',$slug)->first();

        $electrical->electric_new_construction = $request->get('electric_new_construction');
        $electrical->electric_erection = $request->get('electric_erection');
        $electrical->electric_addition = $request->get('electric_addition');
        $electrical->electric_alteration = $request->get('electric_alteration');
        $electrical->electric_ancillary_structure = $request->get('electric_ancillary_structure');
        $electrical->electric_renovation = $request->get('electric_renovation');
        $electrical->electric_repair = $request->get('electric_repair');
        $electrical->electric_moving = $request->get('electric_moving');
        $electrical->electric_demolition = $request->get('electric_demolition');
        $electrical->electric_others = $request->get('electric_others');
        $electrical->total_connected_load = strtoupper($request->get('total_connected_load'));
        $electrical->total_transformer = strtoupper($request->get('total_transformer'));
        $electrical->total_generator_capacity = strtoupper($request->get('total_generator_capacity'));
        $electrical->design_electric_last_name = strtoupper($request->get('design_electric_last_name'));
        $electrical->design_electric_first_name = strtoupper($request->get('design_electric_first_name'));
        $electrical->design_electric_middle_name = strtoupper($request->get('design_electric_middle_name'));
        $electrical->design_electric_prc = strtoupper($request->get('design_electric_prc'));
        $electrical->design_electric_validity = strtoupper($request->get('design_electric_validity'));
        $electrical->design_electric_ptr = strtoupper($request->get('design_electric_ptr'));
        $electrical->design_electric_data_issued =strtoupper( $request->get('design_electric_data_issued'));
        $electrical->design_electric_issued_at = strtoupper($request->get('design_electric_issued_at'));
        $electrical->design_electric_tin = strtoupper($request->get('design_electric_tin'));
        $electrical->design_electric_address =strtoupper( $request->get('design_electric_address'));
        $electrical->prof_electrical = $request->get('prof_electrical');
        $electrical->reg_electrical = $request->get('reg_electrical');
        $electrical->reg_master = $request->get('reg_master');
        $electrical->supervisor_electric_last_name = strtoupper($request->get('supervisor_electric_last_name'));
        $electrical->supervisor_electric_first_name = strtoupper($request->get('supervisor_electric_first_name'));
        $electrical->supervisor_electric_middle_name = strtoupper($request->get('supervisor_electric_middle_name'));
        $electrical->supervisor_electric_prc = strtoupper($request->get('supervisor_electric_prc'));
        $electrical->supervisor_electric_validity =strtoupper($request->get('supervisor_electric_validity'));
        $electrical->supervisor_electric_ptr = strtoupper($request->get('supervisor_electric_ptr'));
        $electrical->supervisor_electric_data_issued = strtoupper($request->get('supervisor_electric_data_issued'));
        $electrical->supervisor_electric_issued_at = strtoupper($request->get('supervisor_electric_issued_at'));
        $electrical->supervisor_electric_tin = strtoupper($request->get('supervisor_electric_tin'));
        $electrical->supervisor_electric_address = strtoupper($request->get('supervisor_electric_address'));

        $electrical->update();

        return response()->json($electrical);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Electricalform  $electrical
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $ids =$request->get('id');
        $application = Electricalform::find($ids)->each(function ($application, $key) {
            $application->delete();
        });
        return response()->json($application);
    }
}
