<?php

namespace App\Http\Controllers\Api;

use App\Models\Sanitaryform;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SanitaryformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sanitary = Sanitaryform::all();
        return response()->json($sanitary);
    }
    public function client($id)
    {
        $sanitary = Sanitaryform::where('user_id', $id)->get();
        return response()->json($sanitary);
    }
    public function slug($slug)
    {
        $sanitary = Sanitaryform::where('tracking_id', $slug)->first();
        return response()->json($sanitary);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sanitary_story_building'=> 'required',
            'sanitary_total_area'=> 'required',
            'sanitary_date_start'=> 'required',
            'sanitary_total_cost'=> 'required',
            'sanitary_expected_date'=> 'required',
            'sanitary_eng_last_name'=> 'required',
            'sanitary_eng_first_name'=> 'required',
            'sanitary_eng_middle_name'=> 'required',
            'sanitary_eng_prc'=> 'required',
            'sanitary_eng_validity'=> 'required',
            'sanitary_eng_ptr'=> 'required',
            'sanitary_eng_data_issued'=> 'required',
            'sanitary_eng_issued_at'=> 'required',
            'sanitary_eng_tin'=> 'required',
            'sanitary_eng_address'=> 'required',
        ]);
        
        $sanitary = new Sanitaryform();

        $sanitary->user_id= $request->get('user_id');
        $sanitary->tracking_id= $request->get('tracking_id');
        $sanitary->sanitary_new_installation= $request->get('sanitary_new_installation');
        $sanitary->sanitary_addition_of= $request->get('sanitary_addition_of');
        $sanitary->sanitary_repair_of= $request->get('sanitary_repair_of');
        $sanitary->sanitary_removal_of= $request->get('sanitary_removal_of');
        $sanitary->sanitary_others= $request->get('sanitary_others');
        $sanitary->sanitary_story_building= $request->get('sanitary_story_building');
        $sanitary->sanitary_total_area= $request->get('sanitary_total_area');
        $sanitary->sanitary_date_start= $request->get('sanitary_date_start');
        $sanitary->sanitary_total_cost= $request->get('sanitary_total_cost');
        $sanitary->sanitary_expected_date= $request->get('sanitary_expected_date');
        $sanitary->water_distribution_system= $request->get('water_distribution_system');
        $sanitary->sanitary_sewer_system= $request->get('sanitary_sewer_system');
        $sanitary->storm_drainage_system= $request->get('storm_drainage_system');
        $sanitary->qty_water_closet= $request->get('qty_water_closet');
        $sanitary->new_water_closet= $request->get('new_water_closet');
        $sanitary->exist_water_closet= $request->get('exist_water_closet');
        $sanitary->qty_floor_drain= $request->get('qty_floor_drain');
        $sanitary->new_floor_drain= $request->get('new_floor_drain');
        $sanitary->exist_floor_drain= $request->get('exist_floor_drain');
        $sanitary->qty_lavatories= $request->get('qty_lavatories');
        $sanitary->new_lavatories= $request->get('new_lavatories');
        $sanitary->exist_lavatories= $request->get('exist_lavatories');
        $sanitary->qty_kitchen_sink= $request->get('qty_kitchen_sink');
        $sanitary->new_kitchen_sink= $request->get('new_kitchen_sink');
        $sanitary->exist_kitchen_sink= $request->get('exist_kitchen_sink');
        $sanitary->qty_faucet= $request->get('qty_faucet');
        $sanitary->new_faucet= $request->get('new_faucet');
        $sanitary->exist_faucet= $request->get('exist_faucet');
        $sanitary->qty_shower_head= $request->get('qty_shower_head');
        $sanitary->new_shower_head= $request->get('new_shower_head');
        $sanitary->exist_shower_head= $request->get('exist_shower_head');
        $sanitary->qty_water_meter= $request->get('qty_water_meter');
        $sanitary->new_water_meter= $request->get('new_water_meter');
        $sanitary->exist_water_meter= $request->get('exist_water_meter');
        $sanitary->qty_grease_trap= $request->get('qty_grease_trap');
        $sanitary->new_grease_trap= $request->get('new_grease_trap');
        $sanitary->exist_grease_trap= $request->get('exist_grease_trap');
        $sanitary->qty_bath_tubs= $request->get('qty_bath_tubs');
        $sanitary->new_bath_tubs= $request->get('new_bath_tubs');
        $sanitary->exist_bath_tubs= $request->get('exist_bath_tubs');
        $sanitary->qty_slop_sink= $request->get('qty_slop_sink');
        $sanitary->new_slop_sink= $request->get('new_slop_sink');
        $sanitary->exist_slop_sink= $request->get('exist_slop_sink');
        $sanitary->qty_urinal= $request->get('qty_urinal');
        $sanitary->new_urinal= $request->get('new_urinal');
        $sanitary->exist_urinal= $request->get('exist_urinal');
        $sanitary->qty_air_conditioning= $request->get('qty_air_conditioning');
        $sanitary->new_air_conditioning= $request->get('new_air_conditioning');
        $sanitary->exist_air_conditioning= $request->get('exist_air_conditioning');
        $sanitary->qty_water_tank= $request->get('qty_water_tank');
        $sanitary->new_water_tank= $request->get('new_water_tank');
        $sanitary->exist_water_tank= $request->get('exist_water_tank');
        $sanitary->table_1_total= $request->get('table_1_total');
        $sanitary->qty_bidette= $request->get('qty_bidette');
        $sanitary->new_bidette= $request->get('new_bidette');
        $sanitary->exist_bidette= $request->get('exist_bidette');
        $sanitary->qty_laundry_trays= $request->get('qty_laundry_trays');
        $sanitary->new_laundry_trays= $request->get('new_laundry_trays');
        $sanitary->exist_laundry_trays= $request->get('exist_laundry_trays');
        $sanitary->qty_dental_cuspidor= $request->get('qty_dental_cuspidor');
        $sanitary->new_dental_cuspidor= $request->get('new_dental_cuspidor');
        $sanitary->exist_dental_cuspidor= $request->get('exist_dental_cuspidor');
        $sanitary->qty_electrical_heater= $request->get('qty_electrical_heater');
        $sanitary->new_electrical_heater= $request->get('new_electrical_heater');
        $sanitary->exist_electrical_heater= $request->get('exist_electrical_heater');
        $sanitary->qty_water_boiler= $request->get('qty_water_boiler');
        $sanitary->new_water_boiler= $request->get('new_water_boiler');
        $sanitary->exist_water_boiler= $request->get('exist_water_boiler');
        $sanitary->qty_drinking_fountain= $request->get('qty_drinking_fountain');
        $sanitary->new_drinking_fountain= $request->get('new_drinking_fountain');
        $sanitary->exist_drinking_fountain= $request->get('exist_drinking_fountain');
        $sanitary->qty_bar_sink= $request->get('qty_bar_sink');
        $sanitary->new_bar_sink= $request->get('new_bar_sink');
        $sanitary->exist_bar_sink= $request->get('exist_bar_sink');
        $sanitary->qty_soda_fountain= $request->get('qty_soda_fountain');
        $sanitary->new_soda_fountain= $request->get('new_soda_fountain');
        $sanitary->exist_soda_fountain= $request->get('exist_soda_fountain');
        $sanitary->qty_laboratory_sink= $request->get('qty_laboratory_sink');
        $sanitary->new_laboratory_sink= $request->get('new_laboratory_sink');
        $sanitary->exist_laboratory_sink= $request->get('exist_laboratory_sink');
        $sanitary->qty_sterilizer= $request->get('qty_sterilizer');
        $sanitary->new_sterilizer= $request->get('new_sterilizer');
        $sanitary->exist_sterilizer= $request->get('exist_sterilizer');
        $sanitary->qty_swimming_pool= $request->get('qty_swimming_pool');
        $sanitary->new_swimming_pool= $request->get('new_swimming_pool');
        $sanitary->exist_swimming_pool= $request->get('exist_swimming_pool');
        $sanitary->qty_sanitary_specify= $request->get('qty_sanitary_specify');
        $sanitary->qty_sanitary_others= $request->get('qty_sanitary_others');
        $sanitary->new_sanitary_others= $request->get('new_sanitary_others');
        $sanitary->exist_sanitary_others= $request->get('exist_sanitary_others');
        $sanitary->table_2_total= $request->get('table_2_total');
        $sanitary->sanitary_shallow_well= $request->get('sanitary_shallow_well');
        $sanitary->sanitary_deep_well= $request->get('sanitary_deep_well');
        $sanitary->sanitary_water_system= $request->get('sanitary_water_system');
        $sanitary->sanitary_others_water= $request->get('sanitary_others_water');
        $sanitary->sanitary_water_waste= $request->get('sanitary_water_waste');
        $sanitary->sanitary_septic_vault= $request->get('sanitary_septic_vault');
        $sanitary->sanitary_subsurface= $request->get('sanitary_subsurface');
        $sanitary->sanitary_sewer= $request->get('sanitary_sewer');
        $sanitary->sanitary_surface= $request->get('sanitary_surface');
        $sanitary->sanitary_street_canal= $request->get('sanitary_street_canal');
        $sanitary->sanitary_water_course= $request->get('sanitary_water_course');
        $sanitary->sanitary_eng_last_name= $request->get('sanitary_eng_last_name');
        $sanitary->sanitary_eng_first_name= $request->get('sanitary_eng_first_name');
        $sanitary->sanitary_eng_middle_name= $request->get('sanitary_eng_middle_name');
        $sanitary->sanitary_eng_prc= $request->get('sanitary_eng_prc');
        $sanitary->sanitary_eng_validity= $request->get('sanitary_eng_validity');
        $sanitary->sanitary_eng_ptr= $request->get('sanitary_eng_ptr');
        $sanitary->sanitary_eng_data_issued= $request->get('sanitary_eng_data_issued');
        $sanitary->sanitary_eng_issued_at= $request->get('sanitary_eng_issued_at');
        $sanitary->sanitary_eng_tin= $request->get('sanitary_eng_tin');
        $sanitary->sanitary_eng_address= $request->get('sanitary_eng_address');

        $sanitary->save();

        return response()->json($sanitary);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sanitaryform  $sanitary
     * @return \Illuminate\Http\Response
     */
    public function show(Sanitaryform $sanitary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sanitaryform  $sanitary
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sanitaryform  $sanitary
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Request $request)
    {
        $this->validate($request, [
            'total_connected_load' => 'required',
            'total_transformer' => 'required',
            'total_generator_capacity' => 'required',
            'design_electric_last_name' => 'required',
            'design_electric_first_name' => 'required',
            'design_electric_middle_name' => 'required',
            'design_electric_prc' => 'required',
            'design_electric_validity' => 'required',
            'design_electric_ptr' => 'required',
            'design_electric_data_issued' => 'required',
            'design_electric_issued_at' => 'required',
            'design_electric_tin' => 'required',
            'design_electric_address' => 'required',
            'supervisor_electric_last_name' => 'required',
            'supervisor_electric_first_name' => 'required',
            'supervisor_electric_middle_name' => 'required',
            'supervisor_electric_prc' => 'required',
            'supervisor_electric_validity' => 'required',
            'supervisor_electric_ptr' => 'required',
            'supervisor_electric_data_issued' => 'required',
            'supervisor_electric_issued_at' => 'required',
            'supervisor_electric_tin' => 'required',
            'supervisor_electric_address' => 'required',
        ]);
        
        $sanitary = Sanitaryform::where('tracking_id',$slug)->first();

        $sanitary->electric_new_construction = $request->get('electric_new_construction');
        $sanitary->electric_erection = $request->get('electric_erection');
        $sanitary->electric_addition = $request->get('electric_addition');
        $sanitary->electric_alteration = $request->get('electric_alteration');
        $sanitary->electric_ancillary_structure = $request->get('electric_ancillary_structure');
        $sanitary->electric_renovation = $request->get('electric_renovation');
        $sanitary->electric_repair = $request->get('electric_repair');
        $sanitary->electric_moving = $request->get('electric_moving');
        $sanitary->electric_demolition = $request->get('electric_demolition');
        $sanitary->electric_others = $request->get('electric_others');
        $sanitary->total_connected_load = strtoupper($request->get('total_connected_load'));
        $sanitary->total_transformer = strtoupper($request->get('total_transformer'));
        $sanitary->total_generator_capacity = strtoupper($request->get('total_generator_capacity'));
        $sanitary->design_electric_last_name = strtoupper($request->get('design_electric_last_name'));
        $sanitary->design_electric_first_name = strtoupper($request->get('design_electric_first_name'));
        $sanitary->design_electric_middle_name = strtoupper($request->get('design_electric_middle_name'));
        $sanitary->design_electric_prc = strtoupper($request->get('design_electric_prc'));
        $sanitary->design_electric_validity = strtoupper($request->get('design_electric_validity'));
        $sanitary->design_electric_ptr = strtoupper($request->get('design_electric_ptr'));
        $sanitary->design_electric_data_issued =strtoupper( $request->get('design_electric_data_issued'));
        $sanitary->design_electric_issued_at = strtoupper($request->get('design_electric_issued_at'));
        $sanitary->design_electric_tin = strtoupper($request->get('design_electric_tin'));
        $sanitary->design_electric_address =strtoupper( $request->get('design_electric_address'));
        $sanitary->prof_electrical = $request->get('prof_electrical');
        $sanitary->reg_electrical = $request->get('reg_electrical');
        $sanitary->reg_master = $request->get('reg_master');
        $sanitary->supervisor_electric_last_name = strtoupper($request->get('supervisor_electric_last_name'));
        $sanitary->supervisor_electric_first_name = strtoupper($request->get('supervisor_electric_first_name'));
        $sanitary->supervisor_electric_middle_name = strtoupper($request->get('supervisor_electric_middle_name'));
        $sanitary->supervisor_electric_prc = strtoupper($request->get('supervisor_electric_prc'));
        $sanitary->supervisor_electric_validity =strtoupper($request->get('supervisor_electric_validity'));
        $sanitary->supervisor_electric_ptr = strtoupper($request->get('supervisor_electric_ptr'));
        $sanitary->supervisor_electric_data_issued = strtoupper($request->get('supervisor_electric_data_issued'));
        $sanitary->supervisor_electric_issued_at = strtoupper($request->get('supervisor_electric_issued_at'));
        $sanitary->supervisor_electric_tin = strtoupper($request->get('supervisor_electric_tin'));
        $sanitary->supervisor_electric_address = strtoupper($request->get('supervisor_electric_address'));

        $sanitary->update();

        return response()->json($sanitary);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sanitaryform  $sanitary
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $ids =$request->get('id');
        $application = Sanitaryform::find($ids)->each(function ($application, $key) {
            $application->delete();
        });
        return response()->json($application);
    }
}
