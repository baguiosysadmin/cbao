<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;

class RoleController extends Controller
{
    public function index() 
    {
        $roles = Role::all();
        return response()->json($roles);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
    	
    }
}
