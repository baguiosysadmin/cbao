<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Barangay;

class BarangayController extends Controller
{
    public function index() 
    {
        $barangay = Barangay::orderBy('name', 'asc')->get();
        return response()->json($barangay);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
        
    }
}
