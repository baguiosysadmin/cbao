<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Title;

class TitleController extends Controller
{
    public function index() 
    {
        $titles = Title::all();
        return response()->json($titles);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
    	
    }
}
