<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistElectricaldesign;
use Illuminate\Support\Str;

class ChecklistElectricalDesignController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'elect_design' => 'required',
            'elect_design.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('elect_design');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistElectricaldesign();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistElectricaldesign::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistElectricaldesign::find($id);
        $file->delete();
        return response()->json($file);
    }
}
