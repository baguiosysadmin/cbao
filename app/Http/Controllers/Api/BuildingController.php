<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Building;

class BuildingController extends Controller
{
    public function index() 
    {
        $building = Building::all();
        return response()->json($building);
    }

    public function store(Request $request) 
    {

    }

    public function show($id)
    {
    
    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {
        
    }
}
