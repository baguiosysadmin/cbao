<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistLatestquarter;
use Illuminate\Support\Str;

class ChecklistLatestquarterController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'tax_receipt' => 'required',
            'tax_receipt.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('tax_receipt');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistLatestquarter();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistLatestquarter::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistLatestquarter::find($id);
        $file->delete();
        return response()->json($file);
    }
}
