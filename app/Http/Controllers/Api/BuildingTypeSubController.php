<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\BuildingTypeSub;
use Illuminate\Http\Request;

class BuildingTypeSubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function residential(Request $request)
    {
        $residential = BuildingTypeSub::where('building_type_id',1)->get();
        return response()->json($residential);
    }

    public function commercial(Request $request)
    {
        $commercial = BuildingTypeSub::where('building_type_id',2)->get();
        return response()->json($commercial);
    }

    public function industrial(Request $request)
    {
        $industrial = BuildingTypeSub::where('building_type_id',3)->get();
        return response()->json($industrial);
    }

    public function institutional(Request $request)
    {
        $institutional = BuildingTypeSub::where('building_type_id',4)->get();
        return response()->json($institutional);
    }

    public function agricultural(Request $request)
    {
        $agricultural = BuildingTypeSub::where('building_type_id',5)->get();
        return response()->json($agricultural);
    }

    public function street(Request $request)
    {
        $street = BuildingTypeSub::where('building_type_id',6)->get();
        return response()->json($street);
    }
}
