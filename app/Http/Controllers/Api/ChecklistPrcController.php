<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistPrcid;
use Illuminate\Support\Str;

class ChecklistPrcController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'prc_id' => 'required',
            'prc_id.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('prc_id');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistPrcid();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistPrcid::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistPrcid::find($id);
        $file->delete();
        return response()->json($file);
    }
}
