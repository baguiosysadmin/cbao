<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tracking;

class TrackingController extends Controller
{
    public function index() 
    {
       /* $trackings = Tracking::with('application', 'user', 'status')->get();
        return response()->json($trackings);*/
    }

    public function store(Request $request) 
    {
       $this->validate($request, [
            'application_id' => 'required',
            'status_id' => 'required',
            'user_id' => 'required',
            /*'designation_id' => 'required'*/
        ]);
        
        $tracking = new Tracking();

        $tracking->application_id = $request->get('application_id');
        $tracking->user_id = $request->get('user_id');
        $tracking->status_id = $request->get('status_id');
        $tracking->designation_id = $request->get('designation_id');

        $tracking->save();

        return response()->json($tracking);
    }

    public function show($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['application_id', $id]])->orderBy('updated_at')->get();
        $tracking->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return response()->json($tracking);
    }

    public function update(Request $request)
    {
        $id = $request->get('auth');
        $application_id = $request->get('application_id');
        $tracking = Tracking::latest()->where([['user_id', $id],['application_id', $application_id]])->first();
        $tracking->remark = $request->get('remark');
        $tracking->designation_id = $request->get('designation_id');
        $tracking->update();
        return response()->json($tracking);
    }

    public function destroy($id)
    {
        
    }

    public function listremark(Request $request)
    {   
        $tracking = Tracking::where([['user_id', $request->user_id], ['application_id', $request->application_id], ['remark','!=',null]])->get();
        return response()->json($tracking);
    }

    public function print($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['application_id', $id], ['remark', '!=', null]])->orderBy('user_id')->get();
        $tracking->load(['user' => function($query) {
            $query->with('designation');
        }]);
        return response()->json($tracking);
    }

    public function releasing($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 2], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }

    public function receiving($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 3], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }

    public function architectural($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 5], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }

    public function linegrade($id)
    {
       $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 6], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }

    public function structural($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 7], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function electrical($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 8], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function sanitary($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 9], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function mechanical($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 10], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function cbo($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 11], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function asstcbo($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 12], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
    public function chief($id)
    {
        $tracking = Tracking::with('status', 'application','user')->where([['designation_id', 13], ['application_id', $id]])->orderBy('updated_at')->get()->last();
        return response()->json($tracking);
    }
}
