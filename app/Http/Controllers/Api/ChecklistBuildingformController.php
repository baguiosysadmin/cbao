<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistBuildingform;
use Illuminate\Support\Str;

class ChecklistBuildingformController extends Controller
{
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'permit_form' => 'required',
            'permit_form.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('permit_form');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistBuildingform();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistBuildingform::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistBuildingform::find($id);
        $file->delete();
        return response()->json($file);
    }
}
