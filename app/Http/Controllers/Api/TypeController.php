<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailUser;

class TypeController extends Controller
{
    public function index() 
    {
        $type = Type::all();
        return response()->json($type);
    }

   public function send_email()
    {
        $data = array('name' =>"Test test", "body" => "test body");

        Mail::send('email.welcome', $data, function($message){
            $message->to('adambert.lacay@gmail.com', 'Test')
                    ->subject('test subject');
            $message->from('adambert.aguilar@gmail.com','haha');
        });
        echo "email sent";
       // $user_name = 'adambert.lacay@gmail.com';
       // $to = 'adambert.aguilar@gmail.com';
       // Mail::to($to)->send(new MailUser($user_name));
       // return 'Mail sent successfully';
    }
}
