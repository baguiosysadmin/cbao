<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
class AuthController extends Controller
{
    
    public function register(Request $request){

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'title_id' => 'required',
            'role_id' => 'required',
        ]);
        
        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->title_id = $request->title_id;
        $user->role_id = $request->role_id;

        $user->save();

        $request->request->add([
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'P3gQq8S13gjNMq9Yql5M9rHbVNBWYvCrm9eSwScK',
                'username' => $request->email,
                'password' => $request->password,
                'scope' => '',
        ]);

        $tokenRequest = $request->create(
                env('APP_URL').'/oauth/token',
                'post'
        );

        $instance = Route::dispatch($tokenRequest);

       /* return response(['data'=>json_decode((string) $instance->getContent(), true)]);*/
        return response(['data'=>json_decode((string) $instance->getContent(), true),'user'=>$user]);

        
    }

    public function login(Request $request){
        
        $request->validate([
            'email'=>'required',
            'password'=>'required'
        ]);

        $user= User::where([['email',$request->email], ['active',1]])->first();
        if(!$user){
            return response(['status'=>'error','message'=>'User not found']);
        }

        if(Hash::check($request->password, $user->password)){

        $request->request->add([
                'grant_type' => 'password',
                'client_id' => '2',
                'client_secret' => 'P3gQq8S13gjNMq9Yql5M9rHbVNBWYvCrm9eSwScK',
                'username' => $request->email,
                'password' => $request->password,
                'scope' => '',
        ]);

        $tokenRequest = $request->create(
                env('APP_URL').'/oauth/token',
                'post'
        );

        $instance = Route::dispatch($tokenRequest);

        return response(['auth'=>json_decode((string) $instance->getContent(), true),'user'=>$user]);

        
        }else{
            return response(['message'=>'password not match','status'=>'error']);
        }


    }

    public function refreshToken() {

        $request->request->add([
                'grant_type' => 'refresh_token',
                'refresh_token' => request('refresh_token'),
                'client_id' => '2',
                'client_secret' => 'P3gQq8S13gjNMq9Yql5M9rHbVNBWYvCrm9eSwScK',
                'scope' => '',
        ]);

        $tokenRequest = $request->create(
                env('APP_URL').'/oauth/token',
                'post'
        );

        $instance = Route::dispatch($tokenRequest);

        return response(['auth'=>json_decode((string) $instance->getContent(), true),'user'=>$user]);

    }

    public function auth(Request $request) 
    { 
        $user = Auth::user();
        $user->load('title','designation','division','role');
        return response()->json($user);
    }

    public function show($id)
    {
        $user = User::find($id);

        return response()->json($user);
    }

    public function updatepw(Request $request)
    {   
       $this->validate($request, [
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);

        $id = Auth::user()->id;
        $user = User::find($id);
        $user->password = bcrypt($request->get('password'));
        $user->update();
        return response()->json($user);

    }
    public function confirm(User $user, $token) {
        if ($user->confirm($token)) {
            $message = 'You successfully confirmed your e-mail address.';
        } else {
            $message = 'Your e-mail address is either already confirmed or your confirmation token is wrong.';
        }

        return redirect()->route('login')->withMessage($message);
    }
}
