<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Applicant;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applicants = Applicant::all();
        return response()->json($applicants);
    }
    public function client($id)
    {
        $applicants = Applicant::where('user_id', $id)->get();
        return response()->json($applicants);
    }
    public function slug($slug)
    {
        $applicants = Applicant::where('tracking_id', $slug)->first();
        return response()->json($applicants);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'tin' => 'required',
            'barangay_id' => 'required',
            'contact_no' => 'required',
            'email_address' => 'required',
            'home_address' => 'required',
            // 'location_blk_street' => 'required',
            // 'location_tax_dec_no' => 'required',
            'location_barangay' => 'required',
            'type_occupancy' => 'required',
            // 'number_units' => 'required',
            // 'ctc_no' => 'required',
            // 'ctc_date_issued' => 'required',
            // 'place_ctc_issued' => 'required',
        ]);
        
        $applicant = new Applicant();

        $applicant->user_id = $request->get('user_id');
        $applicant->tracking_id = $request->get('tracking_id');
        $applicant->last_name = strtoupper($request->get('last_name'));
        $applicant->first_name = strtoupper($request->get('first_name'));
        $applicant->middle_name = strtoupper($request->get('middle_name'));
        $applicant->tin = $request->get('tin');
        $applicant->enterprise = $request->get('enterprise');
        $applicant->form_ownership = strtoupper($request->get('form_ownership'));
        $applicant->kind_activity = strtoupper($request->get('kind_activity'));
        $applicant->barangay_id = $request->get('barangay_id');
        $applicant->city = $request->get('city');
        $applicant->zip_code = $request->get('zip_code');
        $applicant->contact_no = $request->get('contact_no');
        $applicant->email_address = $request->get('email_address');
        $applicant->home_address = strtoupper($request->get('home_address'));
        $applicant->location_blk_street = strtoupper($request->get('location_blk_street'));
        $applicant->location_tct_no = $request->get('location_tct_no');
        $applicant->location_tax_dec_no = $request->get('location_tax_dec_no');
        $applicant->location_barangay = $request->get('location_barangay');
        $applicant->representative = strtoupper($request->get('representative'));
        $applicant->type_occupancy = $request->get('type_occupancy');
        $applicant->other_type_occupancy = strtoupper($request->get('other_type_occupancy'));
        $applicant->type_occupancy_sub_id = $request->get('type_occupancy_sub_id');
        $applicant->other_type_sub = strtoupper($request->get('other_type_sub'));
        $applicant->number_units = $request->get('number_units');
        $applicant->new_construction = $request->get('new_construction');
        $applicant->additional_of = strtoupper($request->get('additional_of'));
        $applicant->renovation_of = strtoupper($request->get('renovation_of'));
        $applicant->demolition_of = strtoupper($request->get('demolition_of'));
        $applicant->repair_of = strtoupper($request->get('repair_of'));
        $applicant->ctc_no = strtoupper($request->get('ctc_no'));
        $applicant->ctc_date_issued = strtoupper($request->get('ctc_date_issued'));
        $applicant->place_ctc_issued = strtoupper($request->get('place_ctc_issued'));

        $applicant->save();

        return response()->json($applicant);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function show(Applicant $applicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function update($slug, Request $request)
    {
        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'tin' => 'required',
            'barangay_id' => 'required',
            'contact_no' => 'required',
            'email_address' => 'required',
            'home_address' => 'required',
            'location_blk_street' => 'required',
            'location_tax_dec_no' => 'required',
            'location_barangay' => 'required',
            'type_occupancy' => 'required',
            'number_units' => 'required',
            'ctc_no' => 'required',
            'ctc_date_issued' => 'required',
            'place_ctc_issued' => 'required',
        ]);
        
        $applicant = Applicant::where('tracking_id',$slug)->first();

        $applicant->last_name = strtoupper($request->get('last_name'));
        $applicant->first_name = strtoupper($request->get('first_name'));
        $applicant->middle_name = strtoupper($request->get('middle_name'));
        $applicant->tin = $request->get('tin');
        $applicant->enterprise = $request->get('enterprise');
        $applicant->form_ownership = strtoupper($request->get('form_ownership'));
        $applicant->kind_activity = strtoupper($request->get('kind_activity'));
        $applicant->barangay_id = $request->get('barangay_id');
        $applicant->city = $request->get('city');
        $applicant->zip_code = $request->get('zip_code');
        $applicant->contact_no = $request->get('contact_no');
        $applicant->email_address = $request->get('email_address');
        $applicant->home_address = strtoupper($request->get('home_address'));
        $applicant->location_blk_street = strtoupper($request->get('location_blk_street'));
        $applicant->location_tct_no = $request->get('location_tct_no');
        $applicant->location_tax_dec_no = $request->get('location_tax_dec_no');
        $applicant->location_barangay = $request->get('location_barangay');
        $applicant->representative = strtoupper($request->get('representative'));
        $applicant->type_occupancy = $request->get('type_occupancy');
        $applicant->other_type_occupancy = strtoupper($request->get('other_type_occupancy'));
        $applicant->type_occupancy_sub_id = $request->get('type_occupancy_sub_id');
        $applicant->other_type_sub = strtoupper($request->get('other_type_sub'));
        $applicant->number_units = $request->get('number_units');
        $applicant->new_construction = $request->get('new_construction');
        $applicant->additional_of = strtoupper($request->get('additional_of'));
        $applicant->renovation_of = strtoupper($request->get('renovation_of'));
        $applicant->demolition_of = strtoupper($request->get('demolition_of'));
        $applicant->repair_of = strtoupper($request->get('repair_of'));
        $applicant->ctc_no = strtoupper($request->get('ctc_no'));
        $applicant->ctc_date_issued = strtoupper($request->get('ctc_date_issued'));
        $applicant->place_ctc_issued = strtoupper($request->get('place_ctc_issued'));

        $applicant->update();

        return response()->json($applicant);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Applicant  $applicant
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $ids =$request->get('id');
        $application = Applicant::find($ids)->each(function ($application, $key) {
            $application->delete();
        });
        return response()->json($application);
    }
}
