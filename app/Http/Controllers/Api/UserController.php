<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\SignupActivate;

class UserController extends Controller
{
    public function index() 
    {
        $users = User::with('title','role','designation')->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function project() 
    {
        $users = User::with('title','role','designation')->where([['division_id', '!=', 2], ['role_id', '!=', 1]])->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function building() 
    {

        $users = User::with('title','role','designation')->where([['division_id', '!=', 3], ['role_id', '!=', 1]])->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function receiving() 
    {
        $users = User::with('title','role','designation')->where([['designation_id', 3], ['role_id', 2]])->orWhere('designation_id', 11)->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function endorsement()
    {
        $users = User::with('title','role','designation')->where([['designation_id', 13], ['role_id', 3], ['division_id', 3]])->orWhere('designation_id', 11)->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function chief()
    {
        $users = User::with('title','role','designation')->where([['designation_id', 15], ['role_id', 3], ['division_id', 3]])->orWhere([['designation_id', 3],['role_id', 2],['division_id', 3]])->orderBy("designation_id", "ASC")->get();
        return datatables($users)->make(true);
    }

    public function store(Request $request) 
    {
    	$this->validate($request, [
            'email' => 'required|email|unique:users',
            'firstname' => 'required',
            'middlename' => 'required',
            'lastname' => 'required',
            'citizenship' => 'required',
            'contact_no' => 'required',
            'home_address' => 'required',
            'barangay_id' => 'required',
            'password' => 'required|min:6',
            'role_id' => 'required',

    	]);
    	
    	$user = new User();

        $user->email = $request->email;
        $user->firstname = $request->firstname;
        $user->middlename = $request->middlename;
        $user->lastname = $request->lastname;
        $user->citizenship = $request->citizenship;
        $user->contact_no = $request->contact_no;
        $user->home_address = $request->home_address;
        $user->barangay_id = $request->barangay_id;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->role_id;
        $user->activation_token = str_random(60);

    	$user->save();
        $user->notify(new SignupActivate($user));

    	return response()->json($user);

    }

    public function activate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is already invalid.'
            ], 404);
        }
        $user->active = 1;
        $user->activation_token = '';
        $user->save();
        return redirect('/login');
    }
    
    public function show($id){
        $user = User::with('title','role','designation')->find($id);

        return response()->json($user);
    }

    public function update($id, Request $request)
    {
    	$user = User::find($id);
 
    	$user->name = $request->get('name');
    	$user->email = $request->get('email');
        $user->title_id = $request->get('title_id');
        $user->role_id = $request->get('role_id');
        $user->designation_id = $request->get('designation_id');

    	$user->update();
    	return response()->json($user);
    }

    public function destroy($id)
    {
    	$user = User::find($id);

    	$user->delete();

    	return response()->json('User Deleted');
    }
}
