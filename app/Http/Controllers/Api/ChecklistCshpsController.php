<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ChecklistCshp;
use Illuminate\Support\Str;

class ChecklistCshpsController extends Controller
{   
    public function store(Request $request) 
    {   
        $this->validate($request, [
            'cshp' => 'required',
            'cshp.*' => 'mimes:pdf',
        ]);
    	$files = $request->file('cshp');
        $random = Str::random(50);
        $i = 0;
        foreach($files as $file){
            $mime = $file->getMimeType();
            $upload = new ChecklistCshp();
            $pathname = $random . $i . '.' . $file->getClientOriginalExtension();
            $file->move(public_path().'/files/', $pathname);
            $upload->mime_type = $mime;
            $upload->tracking_id = request()->tracking_id;
            $upload->path = $pathname;
            $upload->file = time().$i;
            $upload->filename = $original_name[]=$file->getClientOriginalName();
            $upload->save();
            $i++;
        }
    }
    public function show($id)
    {
        $file = ChecklistCshp::where('tracking_id',$id)->get();
        return response()->json($file);
    }
    public function destroy($id)
    {
        $file = ChecklistCshp::find($id);
        $file->delete();
        return response()->json($file);
    }
}
