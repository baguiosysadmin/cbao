<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\BuildingChecklist;
use App\Models\ChecklistOwner;

class BuildingChecklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BuildingChecklist  $buildingChecklist
     * @return \Illuminate\Http\Response
     */
    public function show(BuildingChecklist $buildingChecklist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BuildingChecklist  $buildingChecklist
     * @return \Illuminate\Http\Response
     */
    public function edit(BuildingChecklist $buildingChecklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BuildingChecklist  $buildingChecklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BuildingChecklist $buildingChecklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BuildingChecklist  $buildingChecklist
     * @return \Illuminate\Http\Response
     */
    public function destroy(BuildingChecklist $buildingChecklist)
    {
        //
    }
}
