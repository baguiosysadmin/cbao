<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\File;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $file = File::all();
        return datatables($file)->make(true);
    }

    public function haha($id)
    {
        $file = File::where('application_id', '=', $id)->get();
        return response()->json($file);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'file' => 'required|file|max:10000024'
        ]);

        $file = "fileName".time().'.'.request()->file->getClientOriginalExtension();

        $upload = new File();

        $upload->filename = request()->file->getClientOriginalName();
        $upload->mime_type =  request()->file->getMimeType();
        $upload->path = $file;
        $upload->application_id = request()->application_id;

        $upload->save();

        $request->file->storeAs('uploads',$file);

        return response()->json($upload);
    }

    public function multiple(Request $request)
    {
        $file = "fileName".time().'.'.request()->file->getClientOriginalExtension();

        $upload = new File();

        $upload->filename = request()->file->getClientOriginalName();
        $upload->mime_type =  request()->file->getMimeType();
        $upload->path = $file;
        $upload->application_id = request()->application_id;

        $upload->save();

        $request->file->storeAs('uploads',$file);

        return response()->json($upload);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function show(File $file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, File $file)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\File  $file
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        //
    }
}
