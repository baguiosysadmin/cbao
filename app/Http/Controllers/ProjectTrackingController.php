<?php

namespace App\Http\Controllers;

use App\projectTracking;
use Illuminate\Http\Request;

class ProjectTrackingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\projectTracking  $projectTracking
     * @return \Illuminate\Http\Response
     */
    public function show(projectTracking $projectTracking)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\projectTracking  $projectTracking
     * @return \Illuminate\Http\Response
     */
    public function edit(projectTracking $projectTracking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\projectTracking  $projectTracking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, projectTracking $projectTracking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\projectTracking  $projectTracking
     * @return \Illuminate\Http\Response
     */
    public function destroy(projectTracking $projectTracking)
    {
        //
    }
}
