<?php

namespace App\Http\Controllers;

use App\Application;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
     public function index() {
        return view("layouts.applications");
    }

    public function create() {
        return view("layouts.application-create");
    }

    public function edit($id) {
        return view("layouts.application-edit", compact('id'));
    }
}
