<?php

namespace App\Http\Controllers;

use App\Electricalform;
use Illuminate\Http\Request;

class ElectricalformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Electricalform  $electricalform
     * @return \Illuminate\Http\Response
     */
    public function show(Electricalform $electricalform)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Electricalform  $electricalform
     * @return \Illuminate\Http\Response
     */
    public function edit(Electricalform $electricalform)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Electricalform  $electricalform
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Electricalform $electricalform)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Electricalform  $electricalform
     * @return \Illuminate\Http\Response
     */
    public function destroy(Electricalform $electricalform)
    {
        //
    }
}
