
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
/*require('./datatables');*/
/*window.application_table = require('./application_datatables');
window.onprocess_table = require('./onprocess_datatables');*/

window.Vue = require('vue');
window.axios = require('axios');
window.moment = require('moment');
window.$ = require('jquery');
window.Bus= new Vue();



/*    window.$ = window.jQuery = require('jquery');
window.dt = require( 'datatables.net' )();
*/
import printJS from 'print-js'
window.printJS = printJS;

import swal from 'sweetalert2'
window.swal = swal;

import VueResource from 'vue-resource'
Vue.use(VueResource)

import VueRouter from 'vue-router'
Vue.use(VueRouter)



/*import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';*/

window.token= localStorage.getItem('token');
window.role= localStorage.getItem('role');
window.division= localStorage.getItem('division');
window.user= localStorage.getItem('user');

axios.defaults.baseURL = process.env.API_LOCATION || 'http://localhost:8000'
axios.defaults.headers.common['Authorization'] = "Bearer "+window.token;
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.headers.get['Content-Type'] = 'application/json';

import App from './views/App'
import ApplicationMonitoring from './views/ApplicationMonitoring'
import Home from './views/Home'
import DashboardPermits from './views/DashboardPermits'
import Application from './views/Application'
import Pending from './views/Pending'


import ComplianceApplication from './views/ComplianceApplication'
import ApprovedApplication from './views/ApprovedApplication'
import EvaluationApplication from './views/EvaluationApplication'
import SortingApplication from './views/SortingApplication'
import ApprovalApplication from './views/ApprovalApplication'
import ReleasedApplication from './views/ReleasedApplication'

import NewApplication from './views/NewApplication'

import ApplicationEdit from './views/ApplicationEdit'
import ApplicationDetails from './views/ApplicationDetails'
import User from './views/User'
import UserCreate from './views/UserCreate'
import UserEdit from './views/UserEdit'
import Track from './views/Track'
import ApplyPermit from './views/ApplyPermit'

import NewProject from './views/NewProject'
import Project from './views/Project'
import EvaluationProject from './views/EvaluationProject'
import ProjectMonitoring from './views/ProjectMonitoring'
import DashboardProjects from './views/DashboardProjects'
import ProjectDetails from './views/ProjectDetails'

import Login from './views/Login'
import Register from './views/Register'
import Logout from './views/Logout'
import Client from './views/Client'
import BuildingPermitForm from './views/BuildingPermitForm'
import ApplicantApplicationsTable from './views/ApplicantApplicationsTable'
import BuildingPermitDetails from './views/BuildingPermitDetails'
import BuildingPermitChecklist from './views/BuildingPermitChecklist'
import ClientApplication from './views/ClientApplication'
import ErrorPage from './views/ErrorPage'

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                requiresVisitors: true
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                requiresVisitors: true
            }
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout,
            meta: {
                requiresNone: true,
            }
        },
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/client',
            name: 'client',
            component: Client,
            props: true,
            meta: {
                requiresClient: true
            }
        },
        {
           path: '/client/new-application',
            name: 'clientnewapplication',
            component: ClientApplication,
            props: true,
            meta: {
                requiresClient: true
            }
        },
        {
            path: '/client/buildingpermit/:slug',
            name: 'buildingpermitchecklist',
            component: BuildingPermitChecklist,
            props: true,
            meta: {
                requiresClient: true
            }
        },
        // {
        //    path: '/client/buildingpermit',
        //     name: 'buildingpermitchecklist',
        //     component: BuildingPermitChecklist,
        //     props: true,
        //     meta: {
        //         requiresAuth: true
        //     }
        // },
        {
            path: '/client/applications',
            name: 'applicanttable',
            component: ApplicantApplicationsTable,
            meta: {
                requiresClient: true
            }
        },
        {
            path: '/client/applications/:slug',
            name: 'buildingdetails',
            component: BuildingPermitDetails,
            props: true,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/building/applicant',
            name: 'applicant',
            component: BuildingPermitForm,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/dashboard/permits',
            name: 'dashboardpermits',
            component: DashboardPermits,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/users',
            name: 'user',
            component: User,
            meta: {
                requiresAuth: true,
            },
            haha: {
                requiresAdmin: true,
            }
        },
         {
            path: '/users/create',
            name: 'user-create',
            component: UserCreate,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/users/:id/edit',
            name: 'user-edit',
            component: UserEdit,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/buildingpermits',
            name: 'buildingpermits',
            component: ApplicationMonitoring
        },
        {
            path: '/applications',
            name: 'applications',
            component: Application,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/pending',
            name: 'pendings',
            component: Pending,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/evaluation',
            name: 'applicationevaluation',
            component: EvaluationApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/sorting',
            name: 'applicationsorting',
            component: SortingApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/approval',
            name: 'applicationapproval',
            component: ApprovalApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/approved',
            name: 'applicationapproved',
            component: ApprovedApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/released',
            name: 'applicationreleased',
            component: ReleasedApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/applications/compliance',
            name: 'applicationcompliance',
            component: ComplianceApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/new-application',
            name: 'new-application',
            component: NewApplication,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        
        {
            path: '/applications/:id',
            name: 'application-details',
            component: ApplicationDetails,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/track',
            name: 'track',
            component: Track,
        },
        {
            path: '/new-project',
            name: 'new-project',
            component: NewProject,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/projects',
            name: 'projects',
            component: Project,
            props: true
        },
        {
            path: '/projects/evaluation',
            name: 'projectevaluation',
            component: EvaluationProject,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/joborders',
            name: 'joborders',
            component: ProjectMonitoring
        },
        {
            path: '/dashboard/projects',
            name: 'dashboardprojects',
            component: DashboardProjects,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/projects/:id',
            name: 'project-details',
            component: ProjectDetails,
            props: true,
            meta: {
                requiresAdmin: true,
            }
        },
        {
            path: '/apply',
            name: 'apply',
            component: ApplyPermit,
            props: true
        },
        {
            path: '*',
            name: 'error',
            component: ErrorPage,
        },
    ],
});


/*router.beforeEach((to, from, next) => {
    if(openRoutes.includes(to.name) ){
        next();
    } else if(window.token){
        next();
    } else {
        next('/login');
    }
})*/

const openRoutes=['login', 'register', 'home', 'track','joborders', 'buildingpermits', 'apply'];
const clients = ['client', 'clientnewapplication']
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresClient)) {
        if (window.token == null) {
            next({
                name: 'login',
            })
        } else if (window.role != 11) {
            next({
                name:'error'
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresAdmin)) {
        if (window.token == null) {
            next({
                name: 'login',
            })
        } else if (window.role == 11) {
            next({
                name:'error'
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresVisitors)) {
        if (window.token != null && window.role != 11) {
            next({
                name: 'dashboardpermits',
            })
        } else if (window.token != null && window.role == 11) {
            next({
                name: 'client',
            })
        } else {
            next()
        }
    } else if (to.matched.some(record => record.meta.requiresNone)) {
        next()
    } else {
        if(openRoutes.includes(to.name) ){
            next();
        } else {
            next('/login');
        }
    }
})

export default router
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*Vue.component('v-select', vSelect)*/

import Select2 from 'v-select2-component';
 
Vue.component('Select2', Select2);

const app = new Vue({
    el: '#app',
    data() {
        return {
            token: token
        }
    },
    components: { App },
    router,
});
