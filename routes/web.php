<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});
Route::get('/dashboard', function () {
    return view('admin');
});

Route::resource('users', 'UserController');
Route::resource('applications', 'ApplicationController');
Route::get('login', 'AuthController@login');
Route::get('register', 'AuthController@register');*/

Route::get('/{any}', 'SpaController@index')->where('any', '.*');

Route::get('/home', 'HomeController@index')->name('home');
