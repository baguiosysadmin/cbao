<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::resource('users', 'Api\UserController');
Route::get('register/activate/{token}', 'Api\UserController@activate');

Route::get('user/building', 'Api\UserController@building');
Route::get('user/project', 'Api\UserController@project');
Route::get('user/receiving', 'Api\UserController@receiving');
Route::get('user/endorsement', 'Api\UserController@endorsement');
Route::get('user/chief', 'Api\UserController@chief');

Route::resource('barangays', 'Api\BarangayController');
Route::resource('buildings', 'Api\BuildingController');
Route::get('queuing', 'Api\ApplicationController@queuing');
Route::post('compliance', 'Api\ApplicationController@compliance');
Route::post('legal', 'Api\ApplicationController@legal');
Route::post('signature', 'Api\ApplicationController@signature');
Route::put('return', 'Api\ApplicationController@return');

Route::post('process', 'Api\ApplicationController@process');
Route::resource('types', 'Api\TypeController');
Route::resource('trackings', 'Api\TrackingController');
Route::get('print/{id}', 'Api\TrackingController@print');
Route::post('listremarks', 'Api\TrackingController@listremark');
Route::put('remark', 'Api\TrackingController@update');
Route::resource('roles', 'Api\RoleController');
Route::resource('designations', 'Api\DesignationController');
Route::resource('divisions', 'Api\DivisionController');
Route::resource('titles', 'Api\TitleController');
Route::resource('statuses', 'Api\StatusController');

Route::resource('files', 'Api\FileController');

Route::post('deficiencies', 'Api\ApplicationController@deficiency');
Route::post('approved', 'Api\ApplicationController@approved');
Route::post('approvals', 'Api\ApplicationController@approval');
Route::post('released', 'Api\ApplicationController@released');
Route::post('evaluations', 'Api\ApplicationController@evaluation');
Route::post('sortings', 'Api\ApplicationController@sorting');
Route::resource('applications', 'Api\ApplicationController');

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('auth', 'Api\AuthController@auth');
	Route::patch('updatepw', 'Api\AuthController@updatepw');
});
/*Route::middleware('auth:api')->get('/auth', 'Api\AuthController@auth');*/

Route::get('project/queuing', 'Api\ProjectController@queuing');
Route::post('project/process', 'Api\ProjectController@process');
Route::post('project/signature', 'Api\ProjectController@signature');
Route::post('project/compliance', 'Api\ProjectController@compliance');
Route::post('project/legal', 'Api\ProjectController@legal');
Route::resource('projects', 'Api\ProjectController');
Route::resource('project/trackings', 'Api\ProjectTrackingController');

Route::get('slip/{id}', 'Api\FileController@haha');


Route::get('remarks/releasing/{id}', 'Api\TrackingController@releasing');
Route::get('remarks/receiving/{id}', 'Api\TrackingController@receiving');
Route::get('remarks/architectural/{id}', 'Api\TrackingController@architectural');
Route::get('remarks/linegrade/{id}', 'Api\TrackingController@linegrade');
Route::get('remarks/structural/{id}', 'Api\TrackingController@structural');
Route::get('remarks/electrical/{id}', 'Api\TrackingController@electrical');
Route::get('remarks/sanitary/{id}', 'Api\TrackingController@sanitary');
Route::get('remarks/mechanical/{id}', 'Api\TrackingController@mechanical');
Route::get('remarks/cbo/{id}', 'Api\TrackingController@cbo');
Route::get('remarks/asstcbo/{id}', 'Api\TrackingController@asstcbo');
Route::get('remarks/chief/{id}', 'Api\TrackingController@chief');

Route::post('multiple-files', 'Api\FileController@multiple');


Route::resource('applicants', 'Api\ApplicantController');
Route::post('applicants/{id}', 'Api\ApplicantController@client');
Route::get('buildingpermit/{slug}', 'Api\ApplicantController@slug');
Route::resource('electricalforms', 'Api\ElectricalformController');
Route::get('electricalform/{slug}', 'Api\ElectricalformController@slug');
Route::resource('sanitaryforms', 'Api\SanitaryformController');
Route::get('sanitaryform/{slug}', 'Api\SanitaryformController@slug');
Route::delete('deletelist', 'Api\ApplicantController@delete');
Route::resource('buildingtypes', 'Api\BuildingTypeController');

Route::get('residential', 'Api\BuildingTypeSubController@residential');
Route::get('commercial', 'Api\BuildingTypeSubController@commercial');
Route::get('industrial', 'Api\BuildingTypeSubController@industrial');
Route::get('institutional', 'Api\BuildingTypeSubController@institutional');
Route::get('agricultural', 'Api\BuildingTypeSubController@agricultural');
Route::get('street', 'Api\BuildingTypeSubController@street');

Route::get('/user/sendmail', 'Api\TypeController@send_email');

Route::resource('owner-files', 'Api\ChecklistOwnerController');
Route::resource('survey-files', 'Api\ChecklistSurveyplanController');
Route::resource('certificationcopy-files', 'Api\ChecklistNotownerController');
Route::resource('taxdec-files', 'Api\ChecklistTaxdeclarationController');
Route::resource('latestquarter-files', 'Api\ChecklistLatestquarterController');
Route::resource('sitepicture-files', 'Api\ChecklistLatestpictureController');
Route::resource('buildingform-files', 'Api\ChecklistBuildingformController');
Route::resource('buildingplan-files', 'Api\ChecklistBuildingplanController');
Route::resource('structuraldesign-files', 'Api\ChecklistStructuraldesignController');
Route::resource('electricdesign-files', 'Api\ChecklistElectricalDesignController');
Route::resource('soilanalysis-files', 'Api\ChecklistSoilanalysisController');
Route::resource('buildingspec-files', 'Api\ChecklistBuildingSpecificationController');
Route::resource('billmaterial-files', 'Api\ChecklistBillmaterialController');
Route::resource('prc-files', 'Api\ChecklistPrcController');
Route::resource('zoning-files', 'Api\ChecklistZoningController');
Route::resource('fire-files', 'Api\ChecklistFireController');
Route::resource('cepmo-files', 'Api\ChecklistCepmoController');
Route::resource('logbook-files', 'Api\ChecklistLogbookController');
Route::resource('tarpaulin-files', 'Api\ChecklistTarpaulinController');
Route::resource('cshp-files', 'Api\ChecklistCshpsController');
Route::resource('letter-files', 'Api\ChecklistLetterController');
Route::resource('clearanceother-files', 'Api\ChecklistClearanceController');
Route::resource('environmentalcertificate-files', 'Api\ChecklistEnvironmentalController');
Route::resource('barangayclearance-files', 'Api\ChecklistBarangayController');

// owner
// survey
// certificationcopy
// taxdec
// taxreceipt
// sitepicture
// permitform
// buildingplan
// structuraldesign
// electricdesign
// soilanalysis
// buildingspec
// billmaterial
// prc
// zoning
// fire
// cepmo
// logbook
// tarpaulin
// cshp
// letter
// clearanceother
// environmentalcertificate
// barangayclearance