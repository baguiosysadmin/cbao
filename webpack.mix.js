let mix = require('laravel-mix');
// require('laravel-mix-bundle-analyzer');
 
// if (!mix.inProduction()) {
//     mix.bundleAnalyzer();
// }
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


mix.js('resources/assets/js/app.js', 'public/js')
   .styles([
    'public/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css',
    'public/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css',
    'public/AdminLTE/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
    'public/AdminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
    'public/AdminLTE/dist/css/AdminLTE.min.css',
    'public/AdminLTE/dist/css/skins/_all-skins.min.css',
    'public/css/style.css',
	],'public/css/app.css')
  .js([
    'public/AdminLTE/bower_components/jquery/dist/jquery.min.js',
    'public/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js',
    'public/AdminLTE/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
    'public/AdminLTE/bower_components/datatables.net/js/jquery.dataTables.min.js',
    'public/AdminLTE/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
    'public/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
    'public/AdminLTE/bower_components/fastclick/lib/fastclick.js',
    'public/AdminLTE/dist/js/adminlte.min.js',
    'public/AdminLTE/dist/js/jQuery.print.js',
    'public/bootstrap-table-master/dist/bootstrap-table.min.js',
    'public/bootstrap-table-master/dist/locale/bootstrap-table-en-US.min.js',
    'public/bootstrap-table-master/dist/extensions/export/bootstrap-table-export.min.js',
    'public/bootstrap-table-master/dist/extensions/filter-control/bootstrap-table-filter-control.min.js',
    'public/bootstrap-table-master/dist/tableExport.min.js',
    'public/js/style.js',
    
 	],'public/js/app.js')
