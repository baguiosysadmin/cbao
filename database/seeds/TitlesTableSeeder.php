<?php

use Illuminate\Database\Seeder;

class TitlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('titles')->insert(array(
        	array(
                'prefix' => 'Mr.',
            ),
            array(
                'name' => 'Ms.',
            ),
            array(
		    	'prefix' => 'Arch.',
		    ),
		   	array(
		       	'name' => 'Engr.',
		    ),
		));
    }
}
