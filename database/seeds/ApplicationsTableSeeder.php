<?php

use Illuminate\Database\Seeder;

class ApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('applications')->insert(array(
        	array(
		    	'name' => 'balh1',
		    	'application_no' => '2018-000-001',
		    	'location' => 1,
		    	'type_id' => 1,
		    	'status_id' => 8,
		    	'building_id' => 1,
		    	'location' => 1,
		    	'barangay_id' => 1,
		    	'official_id' => 1
		    ),
		   	array(
		       	'name' => 'balh1',
		    	'application_no' => '2018-000-002',
		    	'location' => 1,
		    	'type_id' => 1,
		    	'status_id' => 3,
		    	'building_id' => 1,
		    	'location' => 1,
		    	'barangay_id' => 1,
		    	'official_id' => 2
		    ),
		    array(
		       	'name' => 'balh1',
		    	'application_no' => '2018-000-003',
		    	'location' => 1,
		    	'type_id' => 1,
		    	'status_id' => 3,
		    	'building_id' => 1,
		    	'location' => 1,
		    	'barangay_id' => 1,
		    	'official_id' => 3
		    ),
		    array(
		       	'name' => 'balh1',
		    	'application_no' => '2018-000-004',
		    	'location' => 1,
		    	'type_id' => 1,
		    	'status_id' => 3,
		    	'building_id' => 1,
		    	'location' => 1,
		    	'barangay_id' => 1,
		    	'official_id' => 4
		    )
		));
    }
}
