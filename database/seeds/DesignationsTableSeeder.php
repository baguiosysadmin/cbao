<?php

use Illuminate\Database\Seeder;

class DesignationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('designations')->insert(array(
        	array(
		    	'name' => 'System Administrator',
		    	'description' => ''
		    ),
        	array(
		    	'name' => 'Admin (Releasing)',
		    	'description' => ''
		    ),
        	array(
		    	'name' => 'Admin (Receiving)',
		    	'description' => ''
		    ),
		   	array(
		       	'name' => 'Admin (For compliance)',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Architectural',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Line and Grade',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Structural',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Electrical',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Sanitary',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Mechanical',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'City Building Official',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Asst. City Building Official',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Chief Processing',
		       	'description' => ''
		    )
		));
    }
}
