<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert(array(
        	array(
		    	'name' => 'N/A',
		    	'description' => ''
		    ),
		    array(
		    	'name' => 'For sorting',
		    	'description' => 'For sorting'
		    ),
		   	array(
		       	'name' => 'Under Evaluation',
		       	'description' => 'Under Evaluation'
		    ),
		    array(
		       	'name' => 'For compliance',
		       	'description' => 'Deficiency - To be informed'
		    ),
		    array(
		       	'name' => 'For approval',
		       	'description' => 'For Approval - within 5 working days'
		    ),
		    array(
		       	'name' => 'Approved',
		       	'description' => 'Approved'
		    ),
		    array(
		       	'name' => 'Released',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Returned',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Received',
		       	'description' => 'Received'
		    ),
		    array(
		       	'name' => 'Legal Issues',
		       	'description' => 'Legal Issues'
		    )
		));
    }
}
