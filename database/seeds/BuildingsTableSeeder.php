<?php

use Illuminate\Database\Seeder;

class BuildingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buildings')->insert(array(
        	array(
		    	'name' => 'Residential'
		    ),
        	array(
		    	'name' => 'Commercial'
		    ),
		   	array(
		       	'name' => 'Institutional'
		    ),
		    array(
		       	'name' => 'Industrial'
		    ),
		    array(
		       	'name' => 'Not Applicable'
		    )
		));
    }
}
