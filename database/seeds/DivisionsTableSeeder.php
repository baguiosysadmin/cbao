<?php

use Illuminate\Database\Seeder;

class DivisionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('divisions')->insert(array(
        	array(
		    	'name' => 'Not Applicable',
		    ),
        	array(
		    	'name' => 'Building Occupancy Division',
		    ),
		   	array(
		       	'name' => 'Planning, Design and Construction Division',
		     
		    )
		));
    }
}
