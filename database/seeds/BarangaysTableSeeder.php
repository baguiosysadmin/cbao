<?php

use Illuminate\Database\Seeder;

class BarangaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barangays')->insert(array(
        	array(
				'name' => 'ABCR'
			),
			array(
				'name' => 'AZKCO'
			),
			array(
				'name' => 'Alfonso Tabora'
				),
			array(
				'name' => 'Ambiong'
				),
			array(
				'name' => 'Asin Road'
				),
			array(
				'name' => 'Atok Trail'
				),
			array(
				'name' => 'North Central Aurora Hill'
				),
			array(
				'name' => 'South Central Aurora Hill'
				),
			array(
				'name' => 'Bagong Lipunan (Market Area)'
				),
			array(
				'name' => 'BGH Compound'
				),
			array(
				'name' => 'Bakakeng Central'
				),
			array(
				'name' => 'Bakakeng North'
				),
			array(
				'name' => 'Balsigan'
				),
			array(
				'name' => 'Bayan Park Village'
				),
			array(
				'name' => 'Brookspoint'
				),
			array(
				'name' => 'Brookside'
				),
			array(
				'name' => 'Cabinet Hill-Teachers Camp'
				),
			array(
				'name' => 'Camdas Subdivision'
				),
			array(
				'name' => 'Camp 7'
				),
			array(
				'name' => 'Camp 8'
				),
			array(
				'name' => 'Camp Allen'
				),
			array(
				'name' => 'Campo Filipino'
				),
			array(
				'name' => 'Palma-Urbano (Cariño-Palma)'
				),
			array(
				'name' => 'City Camp Central'
				),
			array(
				'name' => 'City Camp Proper'
				),
			array(
				'name' => 'Country Club Village'
				),
			array(
				'name' => 'DPS Area'
				),
			array(
				'name' => 'Dizon Subdivision'
				),
			array(
				'name' => 'Dominican Hill-Mirador'
				),
			array(
				'name' => 'Dontogan'
				),
			array(
				'name' => 'Bayan Park East'
				),
			array(
				'name' => 'Modern Site East'
				),
			array(
				'name' => 'Quirino Hill East'
				),
			array(
				'name' => 'Engineers Hill'
				),
			array(
				'name' => 'Fairview Village'
				),
			array(
				'name' => 'Ferdinand (Happy Homes-Campo Sioco)'
				),
			array(
				'name' => 'Fort del Pilar'
				),
			array(
				'name' => 'Gabriela Silang'
				),
			array(
				'name' => 'Gibraltar'
				),
			array(
				'name' => 'Greenwater Village'
				),
			array(
				'name' => 'Guisad Central'
				),
			array(
				'name' => 'Guisad Sorong'
				),
			array(
				'name' => 'Happy Hollow'
				),
			array(
				'name' => 'Happy Homes (Happy Homes-Lucban)'
				),
			array(
				'name' => 'Harrison-Claudio Carantes'
				),
			array(
				'name' => 'Hillside'
				),
			array(
				'name' => 'Holy Ghost Extension'
				),
			array(
				'name' => 'Holy Ghost Proper'
				),
			array(
				'name' => 'Honeymoon (Honeymoon-Holy Ghost)'
				),
			array(
				'name' => 'Imelda R. Marcos (La Salle)'
				),
			array(
				'name' => 'Imelda Village'
				),
			array(
				'name' => 'Irisan'
				),
			array(
				'name' => 'Kabayanihan'
				),
			array(
				'name' => 'Kagitingan'
				),
			array(
				'name' => 'Kayang Extension'
				),
			array(
				'name' => 'Kayang-Hilltop'
				),
			array(
				'name' => 'Kias'
				),
			array(
				'name' => 'Legarda-Burnham-Kisad'
				),
			array(
				'name' => 'Loakan Proper'
				),
			array(
				'name' => 'Apugan Loakan'
				),
			array(
				'name' => 'Liwanag Loakan'
				),
			array(
				'name' => 'Lopez Jaena'
				),
			array(
				'name' => 'Lourdes Subdivision Extension'
				),
			array(
				'name' => 'Lourdes Subdivision Proper'
				),
			array(
				'name' => 'Andres Bonifacio (Lower Bokawkan)'
				),
			array(
				'name' => 'Dagsian Lower'
				),
			array(
				'name' => 'General Luna Lower'
				),
			array(
				'name' => 'Lower Lourdes Subdivision'
				),
			array(
				'name' => 'Lower Magsaysay'
				),
			array(
				'name' => 'Lower Quirino Hill'
				),
			array(
				'name' => 'General Emilio F. Aguinaldo (Quirino-Magsaysay Lower)'
				),
			array(
				'name' => 'Lower Rock Quarry'
				),
			array(
				'name' => 'Lualhati'
				),
			array(
				'name' => 'Lucnab'
				),
			array(
				'name' => 'Malcolm Square-Perfecto (Jose Abad Santos)'
				),
			array(
				'name' => 'Aurora Hill Proper'
				),
			array(
				'name' => 'Manuel A. Roxas'
				),
			array(
				'name' => 'Bal-Marcoville(Marcoville)'
				),
			array(
				'name' => 'Middle Quezon Hill'
				),
			array(
				'name' => 'Middle Quirino Hill'
				),
			array(
				'name' => 'Middle Rock Quarry'
				),
			array(
				'name' => 'Military Cut-off'
				),
			array(
				'name' => 'Mines View Park'
				),
			array(
				'name' => 'New Lucban'
				),
			array(
				'name' => 'North Sanitary Camp'
				),
			array(
				'name' => 'Outlook Drive'
				),
			array(
				'name' => 'Pacdal'
				),
			array(
				'name' => 'Padre Burgos'
				),
			array(
				'name' => 'Padre Zamora'
				),
			array(
				'name' => 'Phil-Am'
				),
			array(
				'name' => 'Pinget'
				),
			array(
				'name' => 'Pinsao Proper'
				),
			array(
				'name' => 'Pinsao Pilot Project'
				),
			array(
				'name' => 'Magsaysay Private Road'
				),
			array(
				'name' => 'Pucsusan'
				),
			array(
				'name' => 'Poliwes'
				),
			array(
				'name' => 'MRR-Queen of Peace'
				),
			array(
				'name' => 'Quezon Hill Proper'
				),
			array(
				'name' => 'Rizal Monument Area'
				),
			array(
				'name' => 'SLU-SVP Housing Village'
				),
			array(
				'name' => 'Saint Joseph Village'
				),
			array(
				'name' => 'Salud Mitra'
				),
			array(
				'name' => 'San Antonio Village'
				),
			array(
				'name' => 'San Luis Village'
				),
			array(
				'name' => 'San Roque Village'
				),
			array(
				'name' => 'San Vicente'
				),
			array(
				'name' => 'San Escolastica'
				),
			array(
				'name' => 'Santo Rosario'
				),
			array(
				'name' => 'Santo Tomas Proper'
				),
			array(
				'name' => 'Santo Tomas School Area'
				),
			array(
				'name' => 'Scout Barrio'
				),
			array(
				'name' => 'Session Road Area'
				),
			array(
				'name' => 'Slaughter House Area (Santo Niño Slaughter)'
				),
			array(
				'name' => 'South Drive'
				),
			array(
				'name' => 'South Sanitary Camp'
				),
			array(
				'name' => 'Teodora Alonzo'
				),
			array(
				'name' => 'Trancoville'
				),
			array(
				'name' => 'Upper Dagsian'
				),
			array(
				'name' => 'Upper General Luna'
				),
			array(
				'name' => 'Upper Magsaysay'
				),
			array(
				'name' => 'Upper Market Subdivision'
				),
			array(
				'name' => 'Upper Quezon Hill'
				),
			array(
				'name' => 'Quirino-Magasaysay (Upper QM)'
				),
			array(
				'name' => 'Upper Rock Quarry'
				),
			array(
				'name' => 'Victoria Village'
				),
			array(
				'name' => 'West Bayan Park (Bayan Park)'
				),
			array(
				'name' => 'West Modern Site'
				),
			array(
				'name' => 'West Quirino Hill'
				),
		));
    }
}
