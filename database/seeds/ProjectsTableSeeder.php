<?php

use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert(array(
        	array(
		    	'job_order_no' => '2018-000-001',
		    	'client' => 'Adam Bert',
		    	'title' => 'This is project title',
		    	'date_time' => 'September 9, 2018  9:15am',
		    	'status_id' => 1,
		    	'official_id' => 1
		    ),
		));
    }
}