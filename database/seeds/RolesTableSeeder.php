<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
        	array(
		    	'name' => 'Administrator',
		    	'description' => ''
		    ),
        	array(
		    	'name' => 'Receiving',
		    	'description' => ''
		    ),
		   	array(
		       	'name' => 'Processing',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'For Compliance',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Signatory',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Releasing',
		       	'description' => ''
		    ),
		    array(
		       	'name' => 'Legal',
		       	'description' => ''
		    )
		    
		));
    }
}
