<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

	  /*  $this->call([
	        RolesTableSeeder::class
	    ]);
        $this->call([
            ApplicationsTableSeeder::class
        ]);
        $this->call([
            ProjectsTableSeeder::class
        ]);

        $this->call([
            StatusesTableSeeder::class
        ]);

        $this->call([
            TitlesTableSeeder::class
        ]);

        $this->call([
            UsersTableSeeder::class
        ]);

        $this->call([
            TypesTableSeeder::class
        ]);
        
        $this->call([
            DesignationsTableSeeder::class
        ]);
        $this->call([
            BarangaysTableSeeder::class
        ]);
        $this->call([
            BuildingsTableSeeder::class
        ]);*/
         $this->call([
            DivisionsTableSeeder::class
        ]);
    }
}
