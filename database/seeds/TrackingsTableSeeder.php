<?php

use Illuminate\Database\Seeder;

class TrackingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('trackings')->insert(array(
        	array(
		    	'application_id' => '1',
		    	'status_id' => '1',
		    	'user_id' => '1',
		    ),
		   	array(
		       	'application_id' => '2',
		    	'status_id' => '2',
		    	'user_id' => '2',
		    ),
		    array(
		       	'application_id' => '2',
		    	'status_id' => '2',
		    	'user_id' => '2',
		    ),
		    array(
		       	'application_id' => '3',
		    	'status_id' => '3',
		    	'user_id' => '3',
		    ),
		    array(
		       	'application_id' => '4',
		    	'status_id' => '3',
		    	'user_id' => '3',
		    ),
		));
    }
}
