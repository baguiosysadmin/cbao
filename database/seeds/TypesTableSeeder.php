<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert(array(
		    array(
		       	'name' => 'Building Permit',
		    ),
		    array(
		       	'name' => 'Certificate of Occupancy',
		    ),
		    array(
		       	'name' => 'Other Permits',
		    )
		));
    }
}
