<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
		    array(
		       	'title_id' => '1',
		       	'name' => 'Adam Bert Lacay',
		       	'email' =>'admin1@cbao.com',
		       	'role_id' =>'1',
		       	'designation_id' =>'1',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G'
		    ),
		    array(
		       	'title_id' => '1',
		       	'name' => 'Rostom Dayao',
		       	'email' =>'admin2@cbao.com',
		       	'role_id' =>'1',
		       	'designation_id' =>'1',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '3',
		       	'name' => 'Odaney Baguiwen',
		       	'email' =>'arch@cbao.com',
		       	'role_id' =>'3',
		       	'designation_id' =>'5',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '4',
		       	'name' => 'Roger Nawen',
		       	'email' =>'engr@cbao.com',
		       	'role_id' =>'3',
		       	'designation_id' =>'6',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '4',
		       	'name' => 'Nazita Ibanez',
		       	'email' =>'cbo@cbao.com',
		       	'role_id' =>'3',
		       	'designation_id' =>'11',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '1',
		       	'name' => 'Rostom Dayao',
		       	'email' =>'compliance@cbao.com',
		       	'role_id' =>'5',
		       	'designation_id' =>'4',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '1',
		       	'name' => 'Gerald Cabasag',
		       	'email' =>'releasing@cbao.com',
		       	'role_id' =>'4',
		       	'designation_id' =>'2',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '2',
		       	'name' => 'Daisy',
		       	'email' =>'receiving@cbao.com',
		       	'role_id' =>'2',
		       	'designation_id' =>'3',
		       	'division_id' => '2',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '1',
		       	'name' => 'Gerald Cabasag',
		       	'email' =>'releasing@project.com',
		       	'role_id' =>'4',
		       	'designation_id' =>'2',
		       	'division_id' => '3',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    ),
		    array(
		       	'title_id' => '2',
		       	'name' => 'Daisy',
		       	'email' =>'receiving@project.com',
		       	'role_id' =>'2',
		       	'designation_id' =>'3',
		       	'division_id' => '3',
		       	'password' =>'$2y$10$bxiANrvvG/WglNKi1wCe6e6YZxkYf1zTYWKQVWoR7h.d4wFMj7t2G',
		    )
		));
    }
}
