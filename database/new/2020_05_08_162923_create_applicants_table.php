<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applicants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_initial');
            $table->string('tin');
            $table->string('enterprise');
            $table->string('form_ownership');
            $table->string('use_occupancy');
            $table->string('use_occupancy');
            $table->string('address');
            $table->string('barangay_id');
            $table->string('city');
            $table->string('zip_code');
            $table->string('contact_no');
            $table->string('email_address');
            $table->string('lot_no');
            $table->string('blk_no');
            $table->string('tct_no');
            $table->string('tax_dec_no');
            $table->string('street');
            $table->string('barangay');
            $table->string('city');
            $table->string('representative');
            // $table->integer('new_installation');
            // $table->integer('annual_inspection');
            // $table->integer('temporary');
            // $table->integer('reconnection_entrance');
            // $table->integer('separation_entrance');
            // $table->integer('upgrading_entrance');
            // $table->integer('relocation_entrance');
            // $table->string('others');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applicants');
    }
}
